var body = $('body');
var data123 = {};
var totalitemqty = "";
var selectedVendor = '';
var selectedCustomer = '';
var overlay = new Object();



overlay.show = function (addClass) {
	$('.overlay').addClass('on').show();
	if (addClass != undefined) {
		$('.overlay').addClass(addClass);
	}
};
overlay.hide = function () {
	$('.overlay').removeClass('on').hide();
	$('.overlay-hide').hide('fast');
	$('.overlay-remove').remove();
};
body.on('click', '.accountpop', function (e) {
	e.stopPropagation();
});
body.on('click', '.act-my-account', function (e) {
	e.stopPropagation();
	$('.accountpop').css({
		left: 0
	});
});
body.on('click', '.side-barmenu', function (e) {
	e.stopPropagation();
	$('.accountpop').css({
		left: '-275px'
	});
});
$(document).on("click", function () {
	$(".accountpop").css({
		left: '-275px'
	});
});



//DROPDOWN Menu
body.on('click', '.side-menubar', function () {
	const t = $(this);
	t.toggleClass('open');
	t.next().toggle('slow');
	if ($(this).attr('id') == 'menu1') {
		if ($('#menu1').hasClass("open")) {
			$('#list1').css('display', 'block');
			$('#menu2').removeClass('open');
			$('#list2').css('display', 'none');
		} else {
			$('#menu1').removeClass("open");
			$('#list1').css('display', 'none');
		}
	}
	if ($(this).attr('id') == 'menu2') {
		if ($('#menu2').hasClass("open")) {
			$('#list2').css('display', 'block');
			$('#menu1').removeClass('open');
			$('#list1').css('display', 'none');
		} else {
			$('#menu2').removeClass("open");
			$('#list2').css('display', 'none');
		}
	}
});

//menu routing code here
body.on('click', '.act-menu', function () {
	var t = $(this);
	pageName = t.attr('href');
	pageName = pageName.substr(1);


	if (pageName == "dashboard") {
		$(".loggedin").removeClass("hide");
		$(".login").addClass("hide");
		$(".dashboard").removeClass("hide");
		$(".reports").addClass("hide");
		$(".issuetracking").addClass("hide");
		$("#orderbooking").addClass("hide");
		$('.vieworders').addClass('hide');
		$(".orderhistory").addClass("hide");
		$(".viewordershistory").addClass("hide");
		$(".profile-vendor").addClass("hide");
		$(".vchange-password").addClass("hide");
		$(".faq_vendor").addClass("hide");
		getVendorOrdersCount();
		getVendorOrderProductCount();
		getVendorAssignProductCount();
		getVendorShippedProductCount();
	}

	if (pageName == "orderbook") {
		$(".loggedin").removeClass("hide");
		$(".login").addClass("hide");
		$(".dashboard").addClass("hide");
		$(".reports").addClass("hide");
		$(".issuetracking").addClass("hide");
		$("#orderbooking").removeClass("hide");
		$('.vieworders').addClass('hide');
		$(".orderhistory").addClass("hide");
		$(".viewordershistory").addClass("hide");
		$(".profile-vendor").addClass("hide");
		$(".vchange-password").addClass("hide");
		$(".faq_vendor").addClass("hide");
		// getVndOrders();
		// getconfirmedorders();
		// getshippedorders();
		document.getElementById("defaultOpen").click();
	}

	if (pageName == "reports") {
		$(".loggedin").removeClass("hide");
		$(".login").addClass("hide");
		$(".dashboard").addClass("hide");
		$(".reports").removeClass("hide");
		$(".issuetracking").addClass("hide");
		$("#orderbooking").addClass("hide");
		$('.vieworders').addClass('hide');
		$(".orderhistory").addClass("hide");
		$(".viewordershistory").addClass("hide");
		$(".profile-vendor").addClass("hide");
		$(".vchange-password").addClass("hide");
		$(".faq_vendor").addClass("hide");
	}

	if (pageName == "issuetracking") {
		$(".loggedin").removeClass("hide");
		$(".login").addClass("hide");
		$(".dashboard").addClass("hide");
		$(".reports").addClass("hide");
		$(".issuetracking").removeClass("hide");
		$("#orderbooking").addClass("hide");
		$('.vieworders').addClass('hide');
		$(".orderhistory").addClass("hide");
		$(".viewordershistory").addClass("hide");
		$(".profile-vendor").addClass("hide");
		$(".vchange-password").addClass("hide");
		$(".faq_vendor").addClass("hide");
	}

	if (pageName == "orderhistory") {
		$(".loggedin").removeClass("hide");
		$(".login").addClass("hide");
		$(".dashboard").addClass("hide");
		$(".reports").addClass("hide");
		$(".issuetracking").addClass("hide");
		$("#orderbooking").addClass("hide");
		$('.vieworders').addClass('hide');
		$(".orderhistory").removeClass("hide");
		$(".viewordershistory").addClass("hide");
		$(".profile-vendor").addClass("hide");
		$(".vchange-password").addClass("hide");
		$(".faq_vendor").addClass("hide");
		getDispatchVndOrders();
	}

	if (pageName == "profile") {
		$(".loggedin").removeClass("hide");
		$(".login").addClass("hide");
		$(".dashboard").addClass("hide");
		$(".reports").addClass("hide");
		$(".issuetracking").addClass("hide");
		$("#orderbooking").addClass("hide");
		$('.vieworders').addClass('hide');
		$(".orderhistory").addClass("hide");
		$(".viewordershistory").addClass("hide");
		$(".profile-vendor").removeClass("hide");
		$(".vchange-password").addClass("hide");
		$(".faq_vendor").addClass("hide");
		displayprofile();
	}

	if (pageName == "changepassword") {
		$(".loggedin").removeClass("hide");
		$(".login").addClass("hide");
		$(".dashboard").addClass("hide");
		$(".reports").addClass("hide");
		$(".issuetracking").addClass("hide");
		$("#orderbooking").addClass("hide");
		$('.vieworders').addClass('hide');
		$(".orderhistory").addClass("hide");
		$(".viewordershistory").addClass("hide");
		$(".profile-vendor").addClass("hide");
		$(".vchange-password").removeClass("hide");
		$(".faq_vendor").addClass("hide");
	}

	if (pageName == "faqs") {
		$(".loggedin").removeClass("hide");
		$(".login").addClass("hide");
		$(".dashboard").addClass("hide");
		$(".reports").addClass("hide");
		$(".issuetracking").addClass("hide");
		$("#orderbooking").addClass("hide");
		$('.vieworders').addClass('hide');
		$(".orderhistory").addClass("hide");
		$(".viewordershistory").addClass("hide");
		$(".profile-vendor").addClass("hide");
		$(".vchange-password").addClass("hide");
		$(".faq_vendor").removeClass("hide");
	}

	if (pageName == "signout") {
		$(".loggedin").addClass("hide");
		$(".dashboard").addClass("hide");
		$(".reports").addClass("hide");
		$(".issuetracking").addClass("hide");
		$(".small-nav").removeClass("hide");
		$(".login").removeClass("hide");
		$("#orderbooking").addClass("hide");
		$('.vieworders').addClass('hide');
		$(".orderhistory").addClass("hide");
		$(".viewordershistory").addClass("hide");
		$(".profile-vendor").addClass("hide");
		$(".vchange-password").addClass("hide");
		$(".faq_vendor").addClass("hide");
		localStorage.clear();
	}
})


//login form code here
body.on('click', '.loginfom', function () {
	$('.loading').addClass('spinner');
	var email = $("#email").val();
	var password = $("#password1").val();
	if (email == "") {
		var mes = "Please enter Email Id";
		appdata.warningmsg(mes);
		$('.loading').removeClass('spinner');
		return;
	}
	if (password == "") {
		var mes = "Please enter Password";
		appdata.warningmsg(mes);
		$('.loading').removeClass('spinner');
		return;
	}
	appdata.loginform(email, password); //login method
});
//END

// BACK BUTTON FOR PENDING TAB
$('#backtovenords').on('click', function () {
	$(".orderpage").removeClass("hide");
	$('.vieworders').addClass('hide');
	$("#pending-table").removeClass("hide");
	getVndOrders();

})

// BACK BUTTON FOR CONFIRM TAB
$('#backtoconfirmtab').on('click', function () {
	$(".orderpage").removeClass("hide");
	$('.cnfvieworders').addClass('hide');
	$("#accept-table").removeClass("hide");
	getconfirmedorders();
})

// BACK BUTTON FOR SHIPPED TAB
$('#backtoshippedtab').on('click', function () {
	$(".orderpage").removeClass("hide");
	$('.shippedorders').addClass('hide');
	$("#shipped-table").removeClass("hide");
	getshippedorders();
})

//BACK BUTTON FOR VENDOR ORDER HISTORY
$('#backtovenordshistory').on('click', function () {
	$(".orderhistory").removeClass("hide");
	$('.viewordershistory').addClass('hide');
})

//SELECTING ON PENDING TAB IN ORDER BOOKING
$("#defaultOpen").on('click', function () {
	$("#Pending").addClass("showtab");
	$("#Confirmed").removeClass("showtab");
	$("#Shipped").removeClass("showtab");
	getVndOrders();
	$(".orderpage").removeClass("hide");
	$('.vieworders').addClass('hide');
	$("#pending-table").removeClass("hide");
});

//SELECTING ON CONFIRM TAB IN ORDER BOOKING
$("#confirmtabOpen").on('click', function () {
	$("#Confirmed").addClass("showtab");
	$("#Pending").removeClass("showtab");
	$("#Shipped").removeClass("showtab");
	getconfirmedorders();
	$(".orderpage").removeClass("hide");
	$('.cnfvieworders').addClass('hide');
	$("#accept-table").removeClass("hide");
});

//SELECTING ON SHIPPED TAB IN ORDER BOOKING
$("#shippedtabOpen").on('click', function () {
	$("#Shipped").addClass("showtab");
	$("#Confirmed").removeClass("showtab");
	$("#Pending").removeClass("showtab");
	getshippedorders();
	$(".orderpage").removeClass("hide");
	$('.shippedorders').addClass('hide');
	$("#shipped-table").removeClass("hide");

})