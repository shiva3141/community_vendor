$(document).ready(function () {
  localStorage.clear();
});

var appState = [];
var purvendorid = "";
var purcompany = "";
var itemdetails = [];

var igstcal = "";
document.addEventListener("deviceready", init, false);
var appdata = new Object();

function init() {
  $(".content").addClass("login_page");
}

//loginform code start
appdata.loginform = function (email, password) {
  var data = {
    email_id: email,
    password: password
  };

  api.post1("vendorlogin/", data, function (res) {
    if (
      res == "Invaild UserName" ||
      res == "Invalid username or password.Enter correct credentials"
    ) {
      var mes = "Invalid EmailId and Password";
      appdata.errormsg(mes); //DISPLAY MESSAGES
      $(".loading").addClass("spinner"); // HERE LOADER VISIBLE
      $(".loading").removeClass("spinner");
    } else {
      localStorage.setItem(
        "loginname",
        res[0]["first_name"] + " " + res[0]["last_name"]
      );
      localStorage.setItem("vendor_id", res[0]["vendor_id"]);
      localStorage.setItem("userrole", res[0]["role"]);
      localStorage.setItem("emailid", res[0]["email_id"]);

      var mes = "Login Successfully...!";
      appdata.successmsg(mes); //DISPLAY MESSAGES
      $(".loading").addClass("spinner"); // HERE LOADER VISIBLE

      $(".login").addClass("hide"); //LOGIN HIDE HERE
      $(".loggedin").removeClass("hide"); // HEADER HIDE HERE
      $(".accountpop").removeClass("hide"); // HEADER SIDE HIDE HERE
      $(".content").addClass("login_page");
      $(".small-nav").removeClass("hide");

      $(".loading").removeClass("spinner"); //HERE LOADER HIDE

      $(".orderpage").addClass("hide");
      $(".vieworders").addClass("hide");

      $(".dashboard").removeClass("hide");

      getVendorOrdersCount();
      getVendorOrderProductCount();
      getVendorAssignProductCount();
      getVendorShippedProductCount();

    }
  });
};
//loginform code end

//GLOBAL ALERT MESSAGES START

//success messages
appdata.successmsg = function (msg) {
  $("#msg").empty();
  $(".alertMsg").removeClass("hide");
  $(".alertMsg").addClass("sucessMsg");
  $("#msg").append(msg);
  $("#success-alert").show();
  setTimeout(function () {
    $("#success-alert").hide();
  }, 1000);
};

//warning messages
appdata.warningmsg = function (msg) {
  $("#msg").empty();
  $(".alertMsg").removeClass("hide");
  $(".alertMsg").addClass("warningMsg");
  $("#msg").append(msg);
  $("#success-alert").show();
  setTimeout(function () {
    $("#success-alert").hide();
  }, 500);
};

//warning messages
appdata.warningmsg_pwdchng = function (msg) {
  $("#msg").empty();
  $(".alertMsg").removeClass("hide");
  $(".alertMsg").addClass("warningMsg");
  $("#msg").append(msg);
  $("#success-alert").show();
  setTimeout(function () {
    $("#success-alert").hide();
  }, 2000);
};

//error messages
appdata.errormsg = function (msg) {
  $("#msg").empty();
  $(".alertMsg").removeClass("hide");
  $(".alertMsg").addClass("dangerMsg");
  $("#msg").append(msg);
  $("#success-alert").show();
  setTimeout(function () {
    $("#success-alert").hide();
  }, 500);
};

//GLOBAL ALERT MESSAGES END






// get vendor orders(pending)
function getVndOrders() {
  var vndObj = {
    vendor_id: localStorage.getItem("vendor_id")
  };
  var txt = "";
  $("#vendor-orderslist").empty();
  // $(".loading").addClass("spinner");
  api.post1("pendingvendororders/", vndObj, function (res) {
    $(".loading").removeClass("spinner");
    if (res.length > 0) {
      var stat = "'" + 'Pending' + "'";
      for (var i = 0; i < res.length; i++) {
        txt += "<tr>";
        txt +=
          '<th><b class="ui-table-cell-label">Customer Name</b>' +
          res[i].username +
          "</th>";
        txt +=
          '<td><b class="ui-table-cell-label">No of Items</b>' +
          res[i].noofitems +
          "</td>";
        txt +=
          '<td><b class="ui-table-cell-label">Total Amount</b>' +
          res[i].total +
          "</td>";
        txt +=
          '<td><b class="ui-table-cell-label">View Orders</b> <i  class="fa fa-list-alt" style="pointer: cursor" onclick="showList(' +
          res[i].user_id + ',' + stat + ')"></i></td>';

        txt += "</tr>";
        txt += '<br class="hr"></br>';

      }
      $("#vendor-orderslist").append(txt);
      $(".loading").removeClass("spinner");
    } else {
      // $("#vendor-orderslist").empty();
      $(".warnrec").empty();
      $("#vendor-orderslist").after('<span class="warnrec">No records found</span>');
      $(".loading").removeClass("spinner");
    }
  });
}

// get all products to be confirmed
function getconfirmedorders() {
  var vndObj = {
    vendor_id: localStorage.getItem("vendor_id")
  };
  var txt = "";
  $("#vendor-confirmlist").empty();
  // $(".loading").addClass("spinner");
  api.post1("acceptedvendororders/", vndObj, function (res) {
    $(".loading").removeClass("spinner");
    if (res.length > 0) {
      var stat = "'" + 'Accepted' + "'";
      for (var i = 0; i < res.length; i++) {
        txt += "<tr>";
        txt +=
          '<th><b class="ui-table-cell-label">Customer Name</b>' +
          res[i].username +
          "</th>";
        txt +=
          '<td><b class="ui-table-cell-label">No of Items</b>' +
          res[i].noofitems +
          "</td>";
        txt +=
          '<td><b class="ui-table-cell-label">Total Amount</b>' +
          res[i].total +
          "</td>";
        txt +=
          '<td><b class="ui-table-cell-label">View Orders</b> <i  class="fa fa-list-alt" style="pointer: cursor" onclick="showList(' + res[i].user_id + ',' + stat + ')"></i></td>';

        txt += "</tr>";
        txt += '<br class="hr"></br>';
      }
      $("#vendor-confirmlist").append(txt);
      $(".loading").removeClass("spinner");
    } else {
      $(".warnrec").empty();
      $("#vendor-confirmlist").after('<span class="warnrec">No records found</span>');
      $(".loading").removeClass("spinner");
    }
  });
}



// GET ALL PRODUCTS TO BE SHIPPED
function getshippedorders() {
  var vndObj = {
    vendor_id: localStorage.getItem("vendor_id")
  };
  var txt = "";
  $("#vendor-shippedlist").empty();
  // $(".loading").addClass("spinner");
  api.post1("shippedvendororders/", vndObj, function (res) {
    $(".loading").removeClass("spinner");
    if (res.length > 0) {
      var stat = "'" + 'Shipped' + "'";
      for (var i = 0; i < res.length; i++) {
        txt += "<tr>";
        txt +=
          '<th><b class="ui-table-cell-label">Customer Name</b>' +
          res[i].username +
          "</th>";
        txt +=
          '<td><b class="ui-table-cell-label">No of Items</b>' +
          res[i].noofitems +
          "</td>";
        txt +=
          '<td><b class="ui-table-cell-label">Total Amount</b>' +
          res[i].total +
          "</td>";
        txt +=
          '<td><b class="ui-table-cell-label">View Orders</b> <i  class="fa fa-list-alt" style="pointer: cursor" onclick="showList(' + res[i].user_id + ',' + stat + ')"></i></td>';

        txt += "</tr>";
        txt += '<br class="hr"></br>';
      }
      $("#vendor-shippedlist").append(txt);
      $(".loading").removeClass("spinner");
    } else {
      $(".warnrec").empty();
      $("#vendor-shippedlist").after('<span class="warnrec">No records found</span>');
      $(".loading").removeClass("spinner");
    }
  });
}



// VIEW ORDERS BY USERS
function showList(usrId, stat) {

  var usrObj = {
    vendor_id: localStorage.getItem("vendor_id"),
    user_id: usrId
  };

  if (stat == "Pending") {
    $("#pending-table").addClass("hide");
    $(".vieworders").removeClass("hide");
    var usrordlist = "";
    $("#viewordersbyuser").empty();
    $(".loading").addClass("spinner");
    api.post1("vendorpndordersbyuserId/", usrObj, function (res) {
      $(".loading").removeClass("spinner");
      // localStorage.setItem('usrordTbl', res);
      if (res.length > 0) {
        for (var i = 0; i < res.length; i++) {
          usrordlist += "<tr>";
          usrordlist +=
            '<th><b class="ui-table-cell-label">S No</b>' + (i + 1) + "</th>";
          usrordlist +=
            '<td><b class="ui-table-cell-label">Order Id</b>' +
            res[i].order_id +
            "</td>";
          usrordlist +=
            '<td><b class="ui-table-cell-label">Delivery Date</b>' +
            res[i].delivery_date +
            "</td>";
          usrordlist +=
            '<td><b class="ui-table-cell-label">Product Name</b>' +
            res[i].product_name +
            "</td>";
          usrordlist +=
            '<td><b class="ui-table-cell-label">Weight</b>' +
            res[i].weight +
            "</td>";
          usrordlist +=
            '<td><b class="ui-table-cell-label">Cost Per Unit</b>' +
            res[i].costperunit +
            "</td>";
          usrordlist +=
            '<td><b class="ui-table-cell-label">Quantity</b>' +
            res[i].quantity +
            "</td>";
          usrordlist +=
            '<td><b class="ui-table-cell-label">Total Price</b>' +
            res[i].totalprice +
            "</td>";
          usrordlist +=
            '<td><b class="ui-table-cell-label">Actions</b><div class="accept-ord" onclick="acceptorders(' + res[i].order_prod_grouping_id + ',' + res[i].order_detail_id + ',' + res[i].user_id + ',' + res[i].order_id + ')"><a>Accept</a></div> <div class="next-btn" onclick="cancelorder(' + res[i].order_prod_grouping_id + ',' + res[i].order_detail_id + ',' + res[i].user_id + ')"><a>Cancle</a></div>'
          "</td>";
          usrordlist += "</tr>";
          usrordlist += '<br class="hr"></br>';
        }

        $("#viewordersbyuser").append(usrordlist);

      } else {
        $(".warnrec").empty();
        $("#viewordersbyuser").after('<span class="warnrec">No records found</span>');
      }
    });
  }
  else if (stat == "Accepted") {

    $("#accept-table").addClass("hide");
    $(".cnfvieworders").removeClass("hide");
    var acceptlist = "";
    $("#viewconfirmuser").empty();
    $(".loading").addClass("spinner");
    api.post1("vendoracptordersbyuserId/", usrObj, function (res) {
      $(".loading").removeClass("spinner");
      // localStorage.setItem('usrordTbl', res);
      if (res.length > 0) {
        for (var i = 0; i < res.length; i++) {
          acceptlist += "<tr>";
          acceptlist +=
            '<th><b class="ui-table-cell-label">S No</b>' + (i + 1) + "</th>";
          acceptlist +=
            '<td><b class="ui-table-cell-label">Order Id</b>' +
            res[i].order_id +
            "</td>";
          acceptlist +=
            '<td><b class="ui-table-cell-label">Delivery Date</b>' +
            res[i].delivery_date +
            "</td>";
          acceptlist +=
            '<td><b class="ui-table-cell-label">Product Name</b>' +
            res[i].product_name +
            "</td>";
          acceptlist +=
            '<td><b class="ui-table-cell-label">Weight</b>' +
            res[i].weight +
            "</td>";
          acceptlist +=
            '<td><b class="ui-table-cell-label">Cost Per Unit</b>' +
            res[i].costperunit +
            "</td>";
          acceptlist +=
            '<td><b class="ui-table-cell-label">Quantity</b>' +
            res[i].quantity +
            "</td>";
          acceptlist +=
            '<td><b class="ui-table-cell-label">Total Price</b>' +
            res[i].totalprice +
            "</td>";
          acceptlist +=
            '<td><b class="ui-table-cell-label">Actions</b><div class="accept-ord" onclick="dispatchuserorder(' + res[i].order_prod_grouping_id + ',' + res[i].order_detail_id + ',' + res[i].user_id + ')"><a>Dispatch</a></div>'
          "</td>";
          acceptlist += "</tr>";
          acceptlist += '<br class="hr"></br>';
        }

        $("#viewconfirmuser").append(acceptlist);
      } else {
        $(".warnrec").empty();
        $("#viewconfirmuser").after(
          '<span class="warnrec">No records found</span>'
        );
      }
    });

  }
  else if (stat == "Shipped") {
    $("#shipped-table").addClass("hide");
    $(".shippedorders").removeClass("hide");
    var shippedlist = "";
    $("#viewshippeduser").empty();
    $(".loading").addClass("spinner");
    api.post1("vendorshpordersbyuserId/", usrObj, function (res) {
      $(".loading").removeClass("spinner");
      // localStorage.setItem('usrordTbl', res);
      if (res.length > 0) {
        for (var i = 0; i < res.length; i++) {
          shippedlist += "<tr>";
          shippedlist +=
            '<th><b class="ui-table-cell-label">S No</b>' + (i + 1) + "</th>";
          shippedlist +=
            '<td><b class="ui-table-cell-label">Order Id</b>' +
            res[i].order_id +
            "</td>";
          shippedlist +=
            '<td><b class="ui-table-cell-label">Delivery Date</b>' +
            res[i].delivery_date +
            "</td>";
          shippedlist +=
            '<td><b class="ui-table-cell-label">Product Name</b>' +
            res[i].product_name +
            "</td>";
          shippedlist +=
            '<td><b class="ui-table-cell-label">Weight</b>' +
            res[i].weight +
            "</td>";
          shippedlist +=
            '<td><b class="ui-table-cell-label">Cost Per Unit</b>' +
            res[i].costperunit +
            "</td>";
          shippedlist +=
            '<td><b class="ui-table-cell-label">Quantity</b>' +
            res[i].quantity +
            "</td>";
          shippedlist +=
            '<td><b class="ui-table-cell-label">Total Price</b>' +
            res[i].totalprice +
            "</td>";
          shippedlist +=
            '<td><b class="ui-table-cell-label">Actions</b><div class="accept-ord" onclick="deliveruserorder(' + res[i].order_prod_grouping_id + ',' + res[i].order_detail_id + ',' + res[i].user_id + ',' + res[i].order_id + ')"><a>Delivered</a></div>'
          "</td>";
          shippedlist += "</tr>";
          shippedlist += '<br class="hr"></br>';
        }

        $("#viewshippeduser").append(shippedlist);
      } else {
        $(".warnrec").empty();
        $("#viewshippeduser").after(
          '<span class="warnrec">No records found</span>'
        );
      }
    });
  }
}

// DISPATCHING USER ORDERS IN SHIPPED TAB
function deliveruserorder(prod_grpid, orddtid, usrid, ordid) {
  var ordObj = {
    ord_det_id: orddtid,
    order_id: ordid
  }
  api.post1('vendordelvordersbyuserId/', ordObj, function (data) {
    console.log(ordObj)
    var stat = 'Shipped';
    showList(usrid, stat);
  })
}

// CANCEL ORDER IN PENDING TAB
function cancelorder(prod_grpid, orddtid, usrid) {
  console.log('order grouping id :' + prod_grpid + ',' + 'order detail id :' + orddtid + ',' + 'user id' + usrid)
  var cancelobj = {
    group_id: prod_grpid,
    ord_det_id: orddtid,
    user_id: usrid,
  }
  api.post1('cancordrs/', cancelobj, function (data) {
    var stat = 'Pending';
    showList(usrid, stat);
  })

}

// DISPATCH ORDER IN CONIFIRM TAB
function dispatchuserorder(prod_grpid, orddtid, usrid) {
  var ordDet = {
    order_detail_id: orddtid,
    order_prod_grouping_id: prod_grpid,
    vendor_id: localStorage.getItem("vendor_id"),
    user_id: usrid
  }

  api.post1('dispatchorders/', ordDet, function (data) {
    console.log(data);
    var stat = 'Accepted';
    showList(usrid, stat);
  })

}

//accepting vendor pending orders
function acceptorders(prod_grpid, orddtid, usrid, ordid) {
  console.log('order grouping id :' + prod_grpid + ',' + 'order detail id :' + orddtid + ',' + 'user id' + usrid, ordid)
  var grpobj = {
    group_id: prod_grpid,
    ord_det_id: orddtid,
    user_id: usrid,
    order_id: ordid
  }
  api.post1('acceptcordrs/', grpobj, function (data) {
    console.log(data);
    var stat = 'Pending';
    showList(usrid, stat);
  })
}

// to dispatch order
function orderDispatch(usrId) {
  if (!confirm("Do you want to ship the products")) {
    return false;
  } else {
    var usrObj = {
      vendor_id: localStorage.getItem("vendor_id"),
      user_id: usrId
    };
    $(".loading").addClass("spinner");

    api.post1("vendorordersbyuserId/", usrObj, function (res) {
      var ordDet = {
        ordDetails: res
      };
      api.post1("dispatchorders/", ordDet, function (res) {
        $(".loading").removeClass("spinner");
        mes = "Products Shipped";
        appdata.successmsg(mes);
        getVndOrders();
      });
    });
  }
}

// inserting vendor issue
$("#btnissuesubmit").on("click", function () {
  if ($("#txtorderid").val() == "") {
    mes = "Please Enter order id";
    appdata.successmsg(mes);
  } else if ($("#txtproductname").val() == "") {
    mes = "Please Enter Product Name";
    appdata.successmsg(mes);
  } else if ($("#txtsubject").val() == "") {
    mes = "Please Enter Subject";
    appdata.successmsg(mes);
  } else if ($("#txtdescription").val() == "") {
    mes = "Please Enter Description";
    appdata.successmsg(mes);
  } else {
    var userRole = localStorage.getItem("userrole");
    // var userId = localStorage.getItem('user_id');
    var usrissue = {
      usrOrdId: $("#txtorderid").val(),
      usrProdName: $("#txtproductname").val(),
      usrSubject: $("#txtsubject").val(),
      usrDescp: $("#txtdescription").val(),
      role: userRole,
      user_id: localStorage.getItem("vendor_id")
    };
    $(".loading").addClass("spinner");
    api.post1("insertusrissue/", usrissue, function (res) {
      if (res == "Inserted") {
        $(".loading").removeClass("spinner");
        $("#txtorderid").val("");
        $("#txtproductname").val("");
        $("#txtsubject").val("");
        $("#txtdescription").val("");
        mes = "Issue is Submitted";
        appdata.successmsg(mes);
      }
    });
  }
});

// show order history
function getDispatchVndOrders() {
  var vndObj1 = {
    vendor_id: localStorage.getItem("vendor_id")
  };
  $("#ord-history").empty();
  var txt = "";
  $(".loading").addClass("spinner");
  api.post1("vnddispatchorders/", vndObj1, function (res) {
    $(".loading").removeClass("spinner");

    if (res.length > 0) {
      for (var i = 0; i < res.length; i++) {
        var d = new Date(res[i].crtdate);
        var month = d.getMonth() + 1;
        var day = d.getDate();

        var today =
          (("" + day).length < 2 ? "0" : "") +
          day +
          "/" +
          (("" + month).length < 2 ? "0" : "") +
          month +
          "/" +
          d.getFullYear();

       
        txt += "<tr>";
        txt +=
          '<th><b class="ui-table-cell-label">Customer Name</b>' +
          res[i].username +
          "</th>";
        txt += '<td><b class="ui-table-cell-label">Date</b>' + today + "</td>";
        txt +=
          '<td><b class="ui-table-cell-label">Product Name</b>' +
          res[i].product_name +
          "</td>";
        txt +=
          '<td><b class="ui-table-cell-label">Totoal Quantity</b>' +
          res[i].total_quantity +
          "</td>";
        txt +=
          '<td><b class="ui-table-cell-label">Unit Price</b>' +
          res[i].price +
          "</td>";
        txt +=
          '<td><b class="ui-table-cell-label">Totoal Amount</b>' +
          res[i].totalprice +
          "</td>";
        // txt +=
        //   '<td><b class="ui-table-cell-label">View Orders</b> <i  class="fa fa-list-alt" style="pointer: cursor" onclick="showvndordhistory(' +
        //   res[i].user_id +
        //   "," +
        //   res[i].order_prod_grouping_id +
        //   ')"></i></td>';
        txt += "</tr>";
        txt += '<br class="hr"></br>';
      }
      $("#ord-history").append(txt);
    } else {
      $(".warnrec").empty();

      $(".orderhistory h4").after(
        '<span class="warnrec">No records found</span>'
      );
    }
  });
}

// view inner order history
function showvndordhistory(usrId, vndgrpId) {
  $(".orderhistory").addClass("hide");
  $(".viewordershistory").removeClass("hide");
  var usrObj = {
    vendor_id: localStorage.getItem("vendor_id"),
    user_id: usrId,
    vndgrp_id: vndgrpId
  };
  var usrordlist = "";
  $("#view-orders-history").empty();
  $(".loading").addClass("spinner");
  api.post1("vnddispatchorderdetails/", usrObj, function (res) {
    $(".loading").removeClass("spinner");
    // localStorage.setItem('usrordTbl', res);
    if (res.length > 0) {
      for (var i = 0; i < res.length; i++) {
        usrordlist += "<tr>";
        usrordlist +=
          '<th><b class="ui-table-cell-label">S No</b>' + (i + 1) + "</th>";
        usrordlist +=
          '<td><b class="ui-table-cell-label">Order Id</b>' +
          res[i].order_id +
          "</td>";
        usrordlist +=
          '<td><b class="ui-table-cell-label">Product Name</b>' +
          res[i].product_name +
          "</td>";
        usrordlist +=
          '<td><b class="ui-table-cell-label">Weight</b>' +
          res[i].weight +
          "</td>";
        usrordlist +=
          '<td><b class="ui-table-cell-label">Cost Per Unit</b>' +
          res[i].costperunit +
          "</td>";
        usrordlist +=
          '<td><b class="ui-table-cell-label">Quantity</b>' +
          res[i].quantity +
          "</td>";
        usrordlist +=
          '<td><b class="ui-table-cell-label">Total Price</b>' +
          res[i].totalprice +
          "</td>";
        usrordlist += "</tr>";
        usrordlist += '<br class="hr"></br>';
      }
      $("#view-orders-history").append(usrordlist);
    } else {
      $(".warnrec").empty();
      $(".viewordershistory h4").after(
        '<span class="warnrec">No records found</span>'
      );
    }
  });
}

function displayprofile() {
  $(".loading").addClass("spinner");
  if (localStorage.getItem("userrole") == "vendor") {
    var vendorobj = {
      vendor_id: localStorage.getItem("vendor_id")
    };
    api.post1("getvendorbyid/", vendorobj, function (res) {
      $(".loading").removeClass("spinner");
      $("#txtvfname").val(res[0]["first_name"]);
      $("#txtvlname").val(res[0]["last_name"]);
      $("#txtvphno").val(res[0]["phone_no"]);
      $("#txtvemailid").val(res[0]["email_id"]);
      $("#txtvaddress").val(res[0]["address"]);
      $("#txtvgstno").val(res[0]["gst_no"]);
      $("#txtvpanno").val(res[0]["pan_no"]);
      $("#txtvpincode").val(res[0]["pincode"]);
      $("#txtvstname").val(res[0]["state_name"]);
      $("#txtvcityname").val(res[0]["city_name"]);
    });
  }
}

$("#btnChangepwd").on("click", function () {
  if ($("#txtvopwd").val() == "" || $("#txtvopwd").val() == undefined) {
    mes = "Please Enter Old Password";
    appdata.warningmsg(mes);
    return;
  }

  if ($("#txtvnpwd").val() == "" || $("#txtvnpwd").val() == undefined) {
    mes = "Please Enter New Password";
    appdata.warningmsg(mes);
    return;
  }

  if ($("#txtvcnfpwd").val() == "" || $("#txtvcnfpwd").val() == undefined) {
    mes = "Please Enter Confirm New Password";
    appdata.warningmsg(mes);
    return;
  }

  var changepwdbj = {
    email_id: localStorage.getItem("emailid"),
    old_password: $("#txtvopwd").val(),
    new_password: $("#txtvnpwd").val(),
    new_re_password: $("#txtvcnfpwd").val()
  };
  $(".loading").addClass("spinner");
  api.post1("changevendorpassword/", changepwdbj, function (res) {
    if (res == "Duplicate") {
      $(".loading").removeClass("spinner");
      mes = "Old and New passwords should not be same";
      appdata.warningmsg(mes);
      return;
    } else {
      $(".loading").removeClass("spinner");
      mes = "Change Password Successfully";
      appdata.successmsg(mes);

      $("#txtvopwd").val("");
      $("#txtvnpwd").val("");
      $("#txtvcnfpwd").val("");
    }
  });
});

function getVendorOrdersCount() {
  var vendorObj = {
    vendor_id: localStorage.getItem('vendor_id')
  }
  $(".loading").addClass("spinner");
  api.post1('getvendorsorderscount/', vendorObj, function (res4) {
    $('#vendor_ordercnt').text(res4[0]['ordercount']);
  });
}

function getVendorOrderProductCount() {
  var vendorObj = {
    vendor_id: localStorage.getItem('vendor_id')
  }
  api.post1('getvendorsordersproductcount/', vendorObj, function (res4) {
    $('#vendor_tot_prodcount').text(res4[0]['orderproduct']);
  });
}

function getVendorAssignProductCount() {
  var vendorObj = {
    vendor_id: localStorage.getItem('vendor_id')
  }
  api.post1('getvendorassignproductcount/', vendorObj, function (res4) {
    $('#vendor_prod_open').text(res4[0]['assignproduct']);
  });
}



function getVendorShippedProductCount() {
  var vendorObj = {
    vendor_id: localStorage.getItem('vendor_id')
  }
  api.post1('getvendorshippedproductcount/', vendorObj, function (res4) {
    $(".loading").removeClass("spinner");
    $('#vendor_prod_inprogress').text(res4[0]['shippedproduct']);
  });
}